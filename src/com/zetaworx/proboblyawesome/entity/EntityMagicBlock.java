package com.zetaworx.proboblyawesome.entity;

import io.netty.buffer.ByteBuf;

import java.util.List;
import java.util.Random;

import com.zetaworx.proboblyawesome.ProboblyAwesomeMod;
import com.zetaworx.proboblyawesome.handler.packet.packets.ThrowPlayerPacket;

import net.minecraft.block.Block;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.util.Vec3;
import net.minecraft.util.Vec3Pool;
import net.minecraft.world.World;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SuppressWarnings("unused")
public class EntityMagicBlock extends Entity implements IEntityAdditionalSpawnData{

	private EntityLivingBase shootingEntity;
	public String owner;

	static final double root2 = Math.sqrt(2);
	
	private int xTile = -1;
	private int yTile = -1;
	private int zTile = -1;
	private int inTile = 0;
	private boolean inGround = false;
	private int ticksAlive;
	private int ticksInAir = 0;
    private EntityLivingBase ownerEntity;
    private String ownerName;
	public int size = 0;
	public int contacts = 0;
	public int sizeLimit = 10;
	public double fireTime = 0;
	public int step = 0;
	
    public EntityMagicBlock(World par1World)
    {
        super(par1World);
        setSize(0.25F, 0.25F);
    }
	
    @SideOnly(Side.CLIENT)
    public boolean isInRangeToRenderDist(double par1)
    {
        double d1 = this.boundingBox.getAverageEdgeLength() * 4.0D;
        d1 *= 64.0D;
        return true;//par1 < d1 * d1; //Always try to render.
    }
    
	public EntityMagicBlock(World w, EntityLivingBase e, float p, int sizeLimit) {
		super(w);
		ownerEntity = e;
		ownerName = e.getCommandSenderName();
		this.sizeLimit = sizeLimit;
		noClip = true;
		shootingEntity = e;
		fireTime = p;
		this.setSize(0.25F, 0.25F);
		setLocationAndAngles(e.posX, e.posY + (double)e.getEyeHeight(), e.posZ, e.rotationYaw, e.rotationPitch);
        this.posX -= (double)(MathHelper.cos(this.rotationYaw / 180.0F * (float)Math.PI) * 0.16F);
        this.posY -= 0.10000000149011612D;
        this.posZ -= (double)(MathHelper.sin(this.rotationYaw / 180.0F * (float)Math.PI) * 0.16F);
        this.setPosition(this.posX, this.posY, this.posZ);
		float f = 0.2F;
		motionX = (double)(-MathHelper.sin(this.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float)Math.PI) * f);
        motionZ = (double)(MathHelper.cos(this.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float)Math.PI) * f);
        motionY = (double)(-MathHelper.sin((this.rotationPitch + 0.0F) / 180.0F * (float)Math.PI) * f);
        this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, 1.5F, 1.0F);
	}

    public void setThrowableHeading(double par1, double par3, double par5, float par7, float par8)
    {
        float f2 = MathHelper.sqrt_double(par1 * par1 + par3 * par3 + par5 * par5);
        par1 /= (double)f2;
        par3 /= (double)f2;
        par5 /= (double)f2;
        par1 += this.rand.nextGaussian() * 0.007499999832361937D * (double)par8;
        par3 += this.rand.nextGaussian() * 0.007499999832361937D * (double)par8;
        par5 += this.rand.nextGaussian() * 0.007499999832361937D * (double)par8;
        par1 *= (double)par7;
        par3 *= (double)par7;
        par5 *= (double)par7;
        this.motionX = par1;
        this.motionY = par3;
        this.motionZ = par5;
        float f3 = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
        this.prevRotationYaw = this.rotationYaw = (float)(Math.atan2(par1, par5) * 180.0D / Math.PI);
        this.prevRotationPitch = this.rotationPitch = (float)(Math.atan2(par3, (double)f3) * 180.0D / Math.PI);
    }
	
	public float getRadius() {
		return size * 0.3F;
	}

	protected void onImpact(MovingObjectPosition mop) {
		
		switch (mop.typeOfHit)
		{
		case BLOCK:
			Block hitBlock = worldObj.getBlock(mop.blockX, mop.blockY, mop.blockZ);
			Block.getIdFromBlock(hitBlock);
			//hitBlock.dropBlockAsItem(worldObj, var1.blockX, var1.blockY, var1.blockZ, Block.getIdFromBlock(hitBlock), worldObj.getBlockMetadata(var1.blockX, var1.blockY, var1.blockZ));
			//worldObj.setBlock(var1.blockX, var1.blockY, var1.blockZ, ((rand.nextInt(10) == 1)?Blocks.fire:Blocks.air));			
			break;
		case ENTITY:
			break;
		case MISS:
			break;
		}
		specialAttack(mop, 5);
	}
	
    /**
     * Sets the velocity to the args. Args: x, y, z
     */
    @SideOnly(Side.CLIENT)
    public void setVelocity(double par1, double par3, double par5)
    {
        motionX = par1;
        motionY = par3;
        motionZ = par5;

        if (prevRotationPitch == 0.0F && prevRotationYaw == 0.0F)
        {
            float f = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
            prevRotationYaw = rotationYaw = (float)(Math.atan2(par1, par5) * 180.0D / Math.PI);
            prevRotationPitch = rotationPitch = (float)(Math.atan2(par3, (double)f) * 180.0D / Math.PI);
        }
    }
    

    
    @SuppressWarnings("unchecked")
	public void movementUpdate()
    {
    	++ticksInAir;
    	Vec3Pool worldVectorPool = worldObj.getWorldVec3Pool();
    	Vec3 positionVector = worldVectorPool.getVecFromPool(posX, posY, posZ);
    	Vec3 nextPositionVector = worldVectorPool.getVecFromPool(posX + motionX, posY + motionY, posZ + motionZ);
    	MovingObjectPosition mop = worldObj.rayTraceBlocks(positionVector, nextPositionVector);
    	if (mop != null)
    	{
    		nextPositionVector = worldVectorPool.getVecFromPool(mop.hitVec.xCoord, mop.hitVec.yCoord, mop.hitVec.zCoord);
    	}
    	if (!worldObj.isRemote)
    	{
    		Entity entity = null;
			List<Entity> list = worldObj.getEntitiesWithinAABBExcludingEntity(this, boundingBox.addCoord(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));
    		double distNull = 0.0D;
    		EntityLivingBase ownah = getOwner();
    		
    		for (int j = 0; j < list.size(); ++j)
    		{
    			Entity entity1 = list.get(j);
    			
    			if (entity1.canBeCollidedWith() && (entity1 != ownah || ticksInAir >= 5))
    			{
    				float f = 0.3F;
    				AxisAlignedBB bb = entity1.boundingBox.expand((double)f, (double)f, (double)f);
    				MovingObjectPosition mop1 = bb.calculateIntercept(positionVector, nextPositionVector);
    				if (mop1 != null)
    				{
    					double distToObj = positionVector.distanceTo(mop1.hitVec);
    					if (distToObj < distNull || distNull == 0.0D)
    					{
    						entity = entity1;
    						distNull = distToObj;
    					}
    				}
    			}
    		}
    		
    		if (entity != null)
    		{
    			mop = new MovingObjectPosition(entity);
    		}
    	}
    	
    	if (mop != null)
    	{
    		if (mop.typeOfHit == MovingObjectType.BLOCK && worldObj.getBlock(mop.blockX, mop.blockY, mop.blockZ) == Blocks.portal)
    		{
    			setInPortal();
    		}
    		else
    		{
    			onImpact(mop);
    		}
    	}
    	posX += motionX;
    	posY += motionY;
    	posZ += motionZ;
    	
    	float rotation = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
    	rotationYaw = (float)(Math.atan2(motionX, motionZ) * 180.0D / Math.PI);
    	
    	for (rotationPitch = (float)(Math.atan2(motionY, (double) rotation) * 180.0D / Math.PI); rotationPitch - prevRotationPitch < -180.0F; prevRotationPitch -= 360.0F)
    	{
    		;
    	}
    	
    	while (rotationPitch - prevRotationPitch >= 180.0F)
    	{
    		prevRotationPitch += 360.0F;
    	}
    	while (rotationYaw - prevRotationYaw < -180.0F)
    	{
    		prevRotationYaw -= 360.0F;
    	}
    	while (rotationYaw - prevRotationYaw >= 180.0F)
    	{
    		prevRotationYaw += 360.0F;
    	}
    	rotationPitch = prevRotationPitch + (rotationPitch - prevRotationPitch) * 0.2F;
    	rotationYaw   = prevRotationYaw 	+ (rotationYaw	  - prevRotationYaw) 	* 0.2F;
    	double f2 = getSpeed(1.0D, 0.05D, 10, 0.8D, 1.5D, 0D);
    	double f3 = 0.0F;
    	double f4 = 0.25F;
    	if (isInWater())
    	{
    		for (int i = 0; i < 4; ++i)
    		{
    			worldObj.spawnParticle("bubble", posX- motionX * f4, posY - motionY * f4, posZ - motionZ * f4, motionX, motionY, motionZ);
    		}
    		f2 = getSpeed(1.0D, 0.05D, 10, 0.8D, 1.5D, 0.30D);
    	}
    	motionX *= f2;
    	motionY *= f2;
    	motionZ *= f2;
    	motionY -= f3;
    	this.setPosition(posX, posY, posZ);
    }
	
    
    public double getSpeed(double start, double incresePer30, int seconds, double min, double max, double bonus) {
    	double temp = start + (incresePer30 * Math.floor(this.fireTime / seconds)) + bonus;
    	if (temp > max) temp = max;
    	if (temp < min) temp = min;
    	
    	return temp;
    }
    
	@Override
	public void onUpdate() {
		lastTickPosX = posX;
        lastTickPosY = posY;
        lastTickPosZ = posZ;
		super.onUpdate();
		movementUpdate();
		size++;
		if (size >= sizeLimit) {
			MovingObjectPosition pos = new MovingObjectPosition((int) posX, (int) posY, (int) posZ, 4, Vec3.createVectorHelper(posX, posY, posZ));
			onImpact(pos);
			setDead();
		}
		setSize(getRadius(), getRadius());
	}


	@SuppressWarnings("unchecked")
	public void specialAttack(MovingObjectPosition var1, int type) {

		EntityLivingBase p = getOwner();

		if (p == null) {
			setDead();
			return;
		}


		switch (type) {
		case 1:
			AxisAlignedBB pool = AxisAlignedBB.getAABBPool().getAABB(
					var1.hitVec.xCoord - getRadius(),
					var1.hitVec.yCoord - getRadius(),
					var1.hitVec.zCoord - getRadius(),
					var1.hitVec.xCoord + getRadius(),
					var1.hitVec.yCoord + getRadius(),
					var1.hitVec.zCoord + getRadius());
			List<EntityLivingBase> entl = worldObj.getEntitiesWithinAABB(EntityLivingBase.class, pool);
			if ((entl != null) && (entl.size() > 0))
				for (EntityLivingBase el : entl)
					if ((el != null) && (el != p)) {
						el.setFire(30);
						if (getOwner() instanceof EntityPlayer) {
							el.attackEntityFrom(DamageSource
									.causePlayerDamage((EntityPlayer) this
											.getOwner()), 1);
							el.attackEntityFrom(DamageSource.onFire, 2);
						} else
							el.attackEntityFrom(DamageSource.onFire, 2);

					}
			if (var1.typeOfHit == MovingObjectType.BLOCK) {
			} else if (var1.typeOfHit == MovingObjectType.ENTITY) {
				if (!(var1.entityHit instanceof EntityLivingBase))
					break;
				if (var1.entityHit == p)
					break;
				var1.entityHit.setFire(30);
				if (p instanceof EntityPlayer) {
					var1.entityHit
					.attackEntityFrom(DamageSource
							.causePlayerDamage((EntityPlayer) p), 1);
					var1.entityHit.attackEntityFrom(DamageSource.onFire, 2);
				} else
					var1.entityHit.attackEntityFrom(DamageSource.onFire, 2);
			}
			break;
		case 2:
			if (!worldObj.isRemote) {
				AxisAlignedBB pool1 = AxisAlignedBB.getAABBPool().getAABB(
						var1.hitVec.xCoord - getRadius(),
						var1.hitVec.yCoord - getRadius(),
						var1.hitVec.zCoord - getRadius(),
						var1.hitVec.xCoord + getRadius(),
						var1.hitVec.yCoord + getRadius(),
						var1.hitVec.zCoord + getRadius());
				List<EntityLivingBase> entl1 = worldObj.getEntitiesWithinAABB(EntityLivingBase.class, pool1);
				if ((entl1 != null) && (entl1.size() > 0))
					for (EntityLivingBase el : entl1)
						if ((el != null) && (el != p)) {
							if (el.isEntityUndead()) {
								el.addPotionEffect(new PotionEffect(
										Potion.moveSlowdown.id, 120, 2));
								el.attackEntityFrom(DamageSource.magic, 4);
							} else {
								el.addPotionEffect(new PotionEffect(
										Potion.poison.id, 120, 2));
								el.addPotionEffect(new PotionEffect(
										Potion.moveSlowdown.id, 120, 2));
							}
							el.addPotionEffect(new PotionEffect(
									Potion.weakness.id, 120, 5));
							if (p instanceof EntityPlayer) {
								el.attackEntityFrom(DamageSource
										.causePlayerDamage((EntityPlayer) p), 1);
								el.attackEntityFrom(DamageSource.drown, 2);
							} else
								el.attackEntityFrom(DamageSource.drown, 2);

						}
				if (var1.typeOfHit == MovingObjectType.BLOCK) {
				} else if (var1.typeOfHit == MovingObjectType.ENTITY) {
					if (var1.entityHit == p)
						break;
					if (var1.entityHit instanceof EntityLivingBase) {
						EntityLivingBase el = (EntityLivingBase) var1.entityHit;
						if (el.isEntityUndead()) {
							if (!worldObj.isRemote) {
								el.addPotionEffect(new PotionEffect(
										Potion.moveSlowdown.id, 120, 2));
								el.attackEntityFrom(DamageSource.magic, 4);
							}
						} else if (!worldObj.isRemote) {
							el.addPotionEffect(new PotionEffect(
									Potion.poison.id, 120, 2));
							el.addPotionEffect(new PotionEffect(
									Potion.moveSlowdown.id, 120, 2));
						}
						if (!worldObj.isRemote)
							el.addPotionEffect(new PotionEffect(
									Potion.weakness.id, 120, 5));
						if (p instanceof EntityPlayer) {
							el.attackEntityFrom(DamageSource
									.causePlayerDamage((EntityPlayer) p), 1);
							el.attackEntityFrom(DamageSource.drown, 2);
						} else
							el.attackEntityFrom(DamageSource.drown, 2);
					}
				}
			}
			break;
		case 3:
			int dmg = (int) (5 + Math.floor(getRadius()));
			pool = AxisAlignedBB.getAABBPool().getAABB(
					var1.hitVec.xCoord - getRadius(),
					var1.hitVec.yCoord - getRadius(),
					var1.hitVec.zCoord - getRadius(),
					var1.hitVec.xCoord + getRadius(),
					var1.hitVec.yCoord + getRadius(),
					var1.hitVec.zCoord + getRadius());
			entl = worldObj.getEntitiesWithinAABB(EntityLivingBase.class, pool);
			if ((entl != null) && (entl.size() > 0))
				for (EntityLivingBase el : entl)
					if ((el != null) && (el != p)) {
						if (p instanceof EntityPlayer)
							el.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer) p), dmg);
						else
							el.attackEntityFrom(DamageSource.anvil, dmg);
						if (!worldObj.isRemote)
							el.addPotionEffect(new PotionEffect(Potion.blindness.id, 120, 2));
					}
			if (var1.typeOfHit == MovingObjectType.ENTITY) {
				if (!(var1.entityHit instanceof EntityLivingBase))
					break;
				EntityLivingBase el = (EntityLivingBase) var1.entityHit;
				if ((el != null) && (el != p)) {
					if (p instanceof EntityPlayer)
						el.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer) p), dmg);
					else
						el.attackEntityFrom(DamageSource.anvil, dmg);
					if (!worldObj.isRemote)
						el.addPotionEffect(new PotionEffect(Potion.blindness.id, 120, 2));
				}
			}

			break;
		case 4:
			pool = AxisAlignedBB.getAABBPool().getAABB(
					var1.hitVec.xCoord - getRadius(),
					var1.hitVec.yCoord - getRadius(),
					var1.hitVec.zCoord - getRadius(),
					var1.hitVec.xCoord + getRadius(),
					var1.hitVec.yCoord + getRadius(),
					var1.hitVec.zCoord + getRadius());
			entl = worldObj.getEntitiesWithinAABB(EntityLivingBase.class, pool);
			if ((entl != null) && (entl.size() > 0))
				for (EntityLivingBase el : entl)
					if ((el != null) && (el != p))
						try {
							double xdir = el.posX - getOwner().posX;
							double zdir = el.posZ - getOwner().posZ;
							double motionX = xdir * 0.1F;
							double motionY = 0.5F;
							double motionZ = zdir * 0.1F;
							el.addVelocity(motionX, motionY, motionZ);
						} catch (Throwable ex) {
						}
			if (var1.typeOfHit == MovingObjectType.ENTITY) {
				if (!(var1.entityHit instanceof EntityLivingBase))
					break;
				EntityLivingBase el = (EntityLivingBase) var1.entityHit;
				if ((el != null) && (el != p))
					try {
						double xdir = el.posX - getOwner().posX;
						double zdir = el.posZ - getOwner().posZ;
						double dmotionX = xdir * 0.1F;
						double dmotionY = 0.5F;
						double dmotionZ = zdir * 0.1F;
						el.addVelocity(dmotionX, dmotionY, dmotionZ);
					} catch (Throwable ex) {
					}
			}
			
			if (p != null && p instanceof EntityPlayer && !p.worldObj.isRemote){
				EntityPlayerMP en = (EntityPlayerMP)p;
				if ((en.getDistanceToEntity(this) < 40) && ((en.rotationPitch > 50) && (en.rotationPitch < 130))) {
	        		ProboblyAwesomeMod.instance.packets.sendTo(new ThrowPlayerPacket(p.getEntityId(), posX, posY, posZ),en);
				}
			}
			break;
		case 5:
			specialAttack(var1, 1);
			specialAttack(var1, 2);
			specialAttack(var1, 3);
			specialAttack(var1, 4);
			break;
		default:
			break;
		}
		//setDead();
	}
	
    public EntityLivingBase getOwner()
    {
        if (ownerEntity == null && ownerName != null && ownerName.length() > 0)
        {
            ownerEntity = worldObj.getPlayerEntityByName(ownerName);
        }

        return ownerEntity;
    }

	protected void entityInit() {}
	

	@Override
	protected void readEntityFromNBT(NBTTagCompound nbt) {
		size = nbt.getInteger("size_");
		sizeLimit = nbt.getInteger("sizeLimit_");
		ownerName = nbt.getString("creator");
		if (ownerName != null && ownerName.length() == 0)
		{
			ownerName = null;
		}
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound nbt) {
		nbt.setInteger("size_", size);
		nbt.setInteger("sizeLimit_", sizeLimit);
		if ((ownerName == null || ownerName.length() == 0) && ownerEntity != null && ownerEntity instanceof EntityPlayer)
        {
            ownerName = ownerEntity.getCommandSenderName();
        }
		nbt.setString("creator", ownerName == null ? "" : ownerName);
	}
	@Override
	public void readSpawnData(ByteBuf data) {
		size = data.readInt();
		step = data.readInt();
		sizeLimit = data.readInt();
		fireTime = data.readDouble();
	}

	@Override
	public void writeSpawnData(ByteBuf data) {
		data.writeInt(size);
		data.writeInt(step);
		data.writeInt(sizeLimit);
		data.writeDouble(fireTime);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		size = nbt.getInteger("size_");
		sizeLimit = nbt.getInteger("sizeLimit_");
		fireTime = nbt.getDouble("fireTime_");
		ownerName = nbt.getString("creator");
		if (ownerName != null && ownerName.length() == 0)
		{
			ownerName = null;
		}

	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setInteger("size_", size);
		nbt.setInteger("sizeLimit_", sizeLimit);
		nbt.setDouble("fireTime_", fireTime);
		if ((ownerName == null || ownerName.length() == 0) && ownerEntity != null && ownerEntity instanceof EntityPlayer)
        {
            ownerName = ownerEntity.getCommandSenderName();
        }
		nbt.setString("creator", ownerName == null ? "" : ownerName);
	}
}
