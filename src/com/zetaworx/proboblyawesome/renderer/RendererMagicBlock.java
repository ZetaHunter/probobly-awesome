package com.zetaworx.proboblyawesome.renderer;

import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Color;





import com.zetaworx.proboblyawesome.entity.EntityMagicBlock;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RendererMagicBlock extends Render {

	private static final ResourceLocation entityResource = new ResourceLocation("zetaworx:weapons/elementum.png");

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		return this.getEntityTexture((EntityMagicBlock)entity);
	}
	
	protected ResourceLocation getEntityTexture(EntityMagicBlock entity) {
		return entityResource;
	}
	
	public void doRender(EntityMagicBlock theEntity, double par2, double par4, double par6, float par8, float par9) {
		this.renderBlockEntity(theEntity, par2, par4, par6, par8, par9);
	}
	
	@Override
	public void doRender(Entity theEntity, double par2, double par4, double par6, float par8, float par9) {
		this.doRender((EntityMagicBlock)theEntity, par2, par4, par6, par8, par9);
	}
	

	public void renderBlockEntity(EntityMagicBlock theEntity, double par2,
			double par4, double par6, float par8, float par9) {

		GL11.glPushMatrix();
		GL11.glTranslatef((float) par2, (float) par4, (float) par6);
		GL11.glRotatef((new Random()).nextInt(360), 1, 0, 0);
		GL11.glRotatef((new Random()).nextInt(360), 0, 1, 0);
		GL11.glRotatef((new Random()).nextInt(360), 0, 0, 1);
		int size = theEntity.size;
		GL11.glScalef(0.0F + (size * 0.3F), 0.0F + (size * 0.3F), 0.0F + (size * 0.3F));
		Minecraft.getMinecraft().renderEngine.bindTexture(entityResource);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE_MINUS_SRC_ALPHA);
		//Color clr = getColor(0.3, 0.2, 0.1, 1, 2, 1, (float) theEntity.step / 2);
		theEntity.step++;
		double cl = (Math.sin(theEntity.step*10)/2)+1;
		GL11.glColor4d(cl, cl, cl, 0.5F);

		if (theEntity.step > 1000)
			theEntity.step = 0;
		Render.renderAABB(AxisAlignedBB.getBoundingBox(-0.5, -0.5, -0.5, 0.5, 0.5, 0.5));
		GL11.glPopMatrix();
	}
	
	public Color getColor(double d, double e, double f, int phase1, int phase2,
			int phase3, float g) {
		int center = 128;
		int width = 127;

		int red = (int) ((Math.sin((d * g) + phase1) * width) + center);
		int grn = (int) ((Math.sin((e * g) + phase2) * width) + center);
		int blu = (int) ((Math.sin((f * g) + phase3) * width) + center);

		return new Color(red, grn, blu);
	}
}
