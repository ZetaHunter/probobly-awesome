package com.zetaworx.proboblyawesome.item;

import com.zetaworx.proboblyawesome.ProboblyAwesomeMod;
import com.zetaworx.proboblyawesome.entity.EntityMagicBlock;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemStaff extends Item {
	protected boolean hasAttacked = false;
	public int maxUse = 150;

	public ItemStaff(int maxUse) {
		this.maxUse = maxUse;
	}

	
    public String getUnwrappedUnlocalizedName(String unlocalizedName)
    {
        return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
    }

    @Override
    public String getUnlocalizedName()
    {
        return String.format("item.%s", getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    @Override
    public String getUnlocalizedName(ItemStack itemStack)
    {
        return String.format("item.%s", getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister)
    {
        this.itemIcon = iconRegister.registerIcon(ProboblyAwesomeMod.RESPREFIX + getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }
    
	
	
	public int getStrenght(EntityPlayer p) {
		return p.experienceLevel == 0 ? 1 : (int) p.experienceLevel;
	}

	public boolean hitEntity(ItemStack item, EntityLivingBase attacker, EntityLivingBase defender) {
		return true;
	}

	@Override
	public boolean onBlockDestroyed(ItemStack par1ItemStack, World par2World, Block b, int par4, int par5, int par6, EntityLivingBase par7EntityLiving) {
		if (b.getBlockHardness(par2World, par4, par5, par6) != 0.0D) {
			// par1ItemStack.damageItem(2, par7EntityLiving); // if it breaks, it's chinees, we don't need chinees stuff.
		}
		return true;
	}

	@Override
	public void onCreated(ItemStack stack, World world, EntityPlayer p) {
	}

	@Override
    public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 72000;
    }
	
	
	
	@Override
	public ItemStack onItemRightClick(ItemStack is, World par2World, EntityPlayer p) {
		p.setItemInUse(is, this.getMaxItemUseDuration(is));
		return is;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer p, int count) {
	}

	@Override
	public void onUpdate(ItemStack stack, World w, Entity e, int par4, boolean par5) {

	}

	@Override
	public void onUsingTick(ItemStack stack, EntityPlayer p, int count) {
		int time = this.getMaxItemUseDuration(stack) - count;
		NBTTagCompound nbt = stack.getTagCompound();
		if (!p.worldObj.isRemote) {
				float var7 = time / 20.0F;
				var7 = ((var7 * var7) + (var7 * 2.0F)) / 3.0F;
				EntityMagicBlock ent = new EntityMagicBlock(p.worldObj, p, var7 * 2, 100);
				p.worldObj.spawnEntityInWorld(ent);
				//ProboblyAwesomeMod.packets.sendToServer(new EntitySpawnPacket(1, var7 * 2, 100));
		}
		stack.setTagCompound(nbt);
	}
}
