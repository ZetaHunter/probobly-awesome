package com.zetaworx.proboblyawesome.modes;

import java.util.HashMap;
import java.util.Map;

import net.minecraftforge.common.config.Property;
import cpw.mods.fml.common.FMLLog;

/**
 * @author ZetaHunter
 * @description A class for mode switcher.
 */
public class Mode {
	/**Protected mod variables*/
	protected String _modeName;
	protected String _modeDesc;
	protected Map<String, Property> _options;
	protected Integer _id;
	
	/**Initializes the mode related stuff.*/
	public Mode(String name, String desc)
	{
		_modeName = name;
		_modeDesc = desc;
		_options = new HashMap<String, Property>();
		_options.put("Basic", new Property("Basic", "I really don't know what to put here...", Property.Type.STRING));
		_id = -1;
	}
	
	public Mode(String name)
	{
		this(name,   "No description");
	}
	
	public Mode(){
		this("None", "No description");
	}
	
	/**Returns this modes current options in form of Map<String, Object>*/
	public Map<String, Property> getOptions() {
		return _options;
	}
	
	/**Returns this modes variable by key*/
	public Object getOption(String key)
	{
		return _options.get(key);
	}
	
	/**Returns this modes name*/
	public String getModeName() {
		return _modeName;
	}
	
	public String getUnlocolizedModeName()
	{
		return _modeName;
	}
	
	/**Returns this modes description*/
	public String getModeDesc() {
		return _modeDesc;
	}
	
	/**Sets value by key*/
	public void setOption(String key, String value, Property.Type type) {
		_options.put(key, new Property(key, value, type));
	}
	
	/**Sets content from cfg A to B*/
	public void setOptions(Map<String, Property> newcfg) {
		if (newcfg != null) {
			if (!newcfg.isEmpty()) {
				_options.putAll(newcfg);
			}
		}
	}
	
	/**Draws all the options buttons.*/
	public void drawOptions() {
		FMLLog.info("The draw options has been called.");
	} 

	/**Sends packet to server with new mode information*/
	public void informServer() {
	}
	
	/**Registers all modes*/
	public static void registerModes(ModeRegistry modes){
		modes.register(0, "None", 		new Mode());
		modes.register(1, "Elementum", 	new ModeElementum());
		modes.register(2, "Prototype", 	new Mode("Prototype", "Never know what happens next"));
		modes.register(3, "Lighting", 	new Mode("Lighting", "Fires lighting beam at target"));
		modes.register(4, "Heal", 		new Mode("Heal", "Heals target, if no target, heals player"));
		modes.setCurrent(0);
	}

	public void setID(Integer id) {
		_id = id;
	}
	
	public Integer getID()
	{
		return _id;
	}
}
