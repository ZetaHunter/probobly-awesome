package com.zetaworx.proboblyawesome.modes;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.config.Property.Type;
import cpw.mods.fml.common.FMLLog;

public class ModeRegistry {

	private Map<Integer, Mode> modeList;
	private Map<String, Integer> alliasList;
	private Integer currentActive;
	private Map<String, Property> generalOptions;

	public static ModeRegistry instance;
	
	public ModeRegistry()
	{
		instance = this;
		modeList = new HashMap<Integer, Mode>();
		alliasList = new HashMap<String, Integer>();
		currentActive = -1;
		generalOptions = new HashMap<String, Property>();
		setOption("TestInt", Integer.toString(1), Type.INTEGER);
		setOption("TestDouble", Double.toString(1.23456D), Type.DOUBLE);
		setOption("TestBool", Boolean.toString(true), Type.BOOLEAN);
		setOption("TestStr", "String", Type.STRING);
		
	}
	
	public void register(Integer id, String Allias, Mode mode)
	{
		String[] ss = new String[1];
		ss[0] = Allias;
		this.register(id, ss, mode);
	}
	
	public void register(Integer id, String[] Alliases, Mode mode)
	{
		if (!modeList.containsKey(id)) {
			mode.setID(id);
			modeList.put(id, mode);
		} else {
			FMLLog.warning("Id %d is already taken.", id);
		}
		if (Alliases.length >= 1) {
			for (int i = 0; i < Alliases.length; ++i)
			{
				if (!alliasList.containsKey(Alliases[i])) {
					alliasList.put(Alliases[i], id);
				} else {
					FMLLog.warning("Allias '%s' is already registred", Alliases[i]);
				}
			}
		}
	}
	
	public Mode get(String id)
	{
		if (alliasList.containsKey(id))
		{
			return this.get(alliasList.get(id));
		} else {
			return null;
		}
	}
	
	public Boolean has(Integer id)
	{
		Boolean temp = false;
		if (modeList.containsKey(id))
		{
			temp = true;
		}
		return temp;
	}
	
	public Boolean has(String name)
	{
		Boolean temp = false;
		if (alliasList.containsKey(name))
		{
			temp = this.has(alliasList.get(name));
		}
		return temp;
	}
	
	public Mode get(Integer id)
	{
		if (modeList.containsKey(id))
		{
			return modeList.get(id);
		} else {
			return null;
		}
	}
	
	public int getSize() {
		return modeList.size();
	}
	
	public Set<Entry<Integer, Mode>> getEntrySet() {
		return modeList.entrySet();
	}

	public Map<String, Property> getOptions() {
		return this.generalOptions;
	}
	
	public Property getOption(String name)
	{
		return this.generalOptions.get(name);
	}

	public void setOptions(Map<String, Property> options) {
		this.generalOptions.putAll(options);
	}
	
	public void setOption(String name, Property option) {
		this.generalOptions.put(name, option);
	}
	
	public void setOption(String name, String value, Type type)	{
		this.setOption(name, new Property(name, value, type));
	}

	public void setCurrent(String name)
	{
		if (alliasList.containsKey(name))
		{
			this.setCurrent(alliasList.get(name));
		}
	}
	
	public void setCurrent(int id)
	{
		if (modeList.containsKey(id))
		{
			currentActive = id;
		} else {
			FMLLog.warning("Attempted to set current mode to missing mode id - %d", id);
		}
	}
	
	public Integer getCurrent()
	{
		return currentActive;
	}
	
	public Mode getCurrentMode() {
		return get(currentActive);
	}
	
}
