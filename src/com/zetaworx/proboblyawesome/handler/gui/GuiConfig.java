package com.zetaworx.proboblyawesome.handler.gui;

import java.util.Map.Entry;

import com.zetaworx.proboblyawesome.modes.ModeRegistry;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.config.Property;

@SideOnly(Side.CLIENT)
public class GuiConfig extends GuiScreen
{
    /**
     * The gui file needs to be 256x256.
     * The GUI ITSELF can have any size you want
     * Define them here
     * These are the GUI sizes!
     */
    GuiConfig.List 		modeButtonList;
    int 				currentSelected;
    Boolean				isCfgForMode;
    
    public GuiConfig(Boolean i) {
    	isCfgForMode = i;
    }

	@SuppressWarnings("unchecked")
	@Override
    public void initGui() {
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(1, 2, 2, 80, 20, "Back"));
        this.buttonList.add(new GuiButton(2, width-80*2, 2, 80, 20, "Save"));
        this.buttonList.add(new GuiButton(3, width-80  , 2, 80, 20, "Reset"));   
        modeButtonList = new GuiConfig.List();
        modeButtonList.registerScrollButtons(4, 5);
    }
    
    protected void actionPerformed(GuiButton btn)
    {
        switch (btn.id)
        {
            case 1:
                this.mc.displayGuiScreen(new GuiStaffCP());
                break;
            case 2:
            	FMLLog.info("This should save to current config");
            	break;
            case 3:
            	FMLLog.info("This should reset to current config");
            	break;
            case 4:
            	modeButtonList.actionPerformed(btn);
            	break;
            case 5:
            	modeButtonList.actionPerformed(btn);
            	break;
            default:
            	break;
        }
    }
    
    public void drawDefaultBackground()
    {
        super.drawDefaultBackground();
    }
    
    public void updateScreen()
    {
        super.updateScreen();
    }
    
    @Override
    public void drawScreen(int par1, int par2, float par3)
    {
        modeButtonList.drawScreen(par1, par2, par3);
        if (isCfgForMode)
        	fontRendererObj.drawString("Mode active: " + ModeRegistry.instance.getCurrentMode().getModeName(), 90, 5, 16777215); 
        super.drawScreen(par1, par2, par3);
    }

    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    
    @SideOnly(Side.CLIENT)
    class List extends GuiMySlot
    {
        public List()
        {
            super(GuiConfig.this.mc, GuiConfig.this.width, GuiConfig.this.height, 24, GuiConfig.this.height - 24, 44);
        }

        protected int getSize()
        {
            if (isCfgForMode)
            {
            	return ModeRegistry.instance.getCurrentMode().getOptions().size();
            } else {
            	return ModeRegistry.instance.getOptions().size();
            }
        }

        /**
         * The element in the slot that was clicked, boolean for whether it was double clicked or not
         */
        protected void elementClicked(int modeId, boolean wasClicked, int proboblyMouseX, int proboblyMouseY)
        {
        	if (isSelected(modeId)) {
        		FMLLog.info("Field %d will now be edited.", modeId);
        	} else {
        		GuiConfig.this.currentSelected = modeId;
        	}
        }

        /**
         * Returns true if the element passed in is currently selected
         */
        protected boolean isSelected(int id)
        {
            return id == GuiConfig.this.currentSelected;
        }

        /**
         * Return the height of the content being scrolled
         */
        protected int getContentHeight()
        {
            return this.getSize() * 44;
        }

        @Override
        protected void drawBackground() {
        }

        @Override
        protected void drawListHeader(int posx, int posy, Tessellator tes) {
        }
        
        @SuppressWarnings("unchecked")
		protected void drawSlot(int id, int posX, int posY, int p_148126_4_, Tessellator tes, int p_148126_6_, int p_148126_7_)
        {
            
			Entry<String, Property> prop;
			if (isCfgForMode){
            	prop = (Entry<String, Property>) ModeRegistry.instance.getCurrentMode().getOptions().entrySet().toArray()[id];
            } else {
            	prop = (Entry<String, Property>) ModeRegistry.instance.getOptions().entrySet().toArray()[id];
            }
            
            String s = prop.getKey();

            if (s == null || MathHelper.stringNullOrLengthZero(s))
            {
                s = "Option " + (id + 1);
            }

            String s1 = prop.getValue().getString();
            
            if (s1 == null || MathHelper.stringNullOrLengthZero(s1))
            {
            	s1 = "(No description)";
            }
            
            GuiConfig.this.fontRendererObj.drawString(s, posX + 2, posY + 1, 16777215);
            
            GuiConfig.this.fontRendererObj.drawString(prop.getValue().getType().name(), this.getListWidth() - 80, posY + 1, 16777215);
            
            GuiConfig.this.fontRendererObj.drawSplitString(s1, posX + 2, posY + 12, this.getListWidth(), 8421504);
            
        }
        
    }    
}