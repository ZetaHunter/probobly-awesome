package com.zetaworx.proboblyawesome.handler.gui;

import com.zetaworx.proboblyawesome.modes.Mode;
import com.zetaworx.proboblyawesome.modes.ModeRegistry;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.MathHelper;

@SideOnly(Side.CLIENT)
public class GuiStaffCP extends GuiScreen
{
    /**
     * The gui file needs to be 256x256.
     * The GUI ITSELF can have any size you want
     * Define them here
     * These are the GUI sizes!
     */
    GuiStaffCP.List 	modeButtonList;
    int 				currentSelected;
    
    @SuppressWarnings("unchecked")
	@Override
    public void initGui() {
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(1, 2, 2, 80, 20, "Close"));
        this.buttonList.add(new GuiButton(2, width-80*2, 2, 80, 20, "Mode CFG"));
        this.buttonList.add(new GuiButton(3, width-80  , 2, 80, 20, "General CFG"));        
        modeButtonList = new GuiStaffCP.List();
        modeButtonList.registerScrollButtons(4, 5);
    }
    
    protected void actionPerformed(GuiButton btn)
    {
        switch (btn.id)
        {
            case 1:
                this.mc.displayGuiScreen((GuiScreen)null);
                this.mc.setIngameFocus();
                break;
            case 2:
            	FMLLog.info("This should open mode confing gui.");
                this.mc.displayGuiScreen(new GuiConfig(true));
            	break;
            case 3:
            	FMLLog.info("This should open general config gui.");
                this.mc.displayGuiScreen(new GuiConfig(false));
            	break;
            case 4:
            	modeButtonList.actionPerformed(btn);
            	break;
            case 5:
            	modeButtonList.actionPerformed(btn);
            	break;
            default:
            	break;
        }
    }
    
    public void drawDefaultBackground()
    {
        super.drawDefaultBackground();
    }
    
    public void updateScreen()
    {
        super.updateScreen();
    }
    
    @Override
    public void drawScreen(int par1, int par2, float par3)
    {
        modeButtonList.drawScreen(par1, par2, par3);
    	fontRendererObj.drawString("Mode active: " + ModeRegistry.instance.getCurrentMode().getModeName(), 90, 5, 16777215); 
        super.drawScreen(par1, par2, par3);
    }

    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    
    @SideOnly(Side.CLIENT)
    class List extends GuiMySlot
    {
        public List()
        {
            super(GuiStaffCP.this.mc, GuiStaffCP.this.width, GuiStaffCP.this.height, 24, GuiStaffCP.this.height - 24, 44);
        }

        protected int getSize()
        {
            return ModeRegistry.instance.getSize();
        }

        /**
         * The element in the slot that was clicked, boolean for whether it was double clicked or not
         */
        protected void elementClicked(int modeId, boolean wasClicked, int proboblyMouseX, int proboblyMouseY)
        {
        	ModeRegistry.instance.setCurrent(modeId);
            GuiStaffCP.this.currentSelected = modeId;
        }

        /**
         * Returns true if the element passed in is currently selected
         */
        protected boolean isSelected(int id)
        {
            return id == GuiStaffCP.this.currentSelected;
        }

        /**
         * Return the height of the content being scrolled
         */
        protected int getContentHeight()
        {
            return this.getSize() * 44;
        }

        @Override
        protected void drawBackground() {
        }

        @Override
        protected void drawListHeader(int posx, int posy, Tessellator tes) {
        }

        
        protected void drawSlot(int id, int posX, int posY, int p_148126_4_, Tessellator tes, int p_148126_6_, int p_148126_7_)
        {
            Mode mode =  (Mode)ModeRegistry.instance.get(id);
            String s = mode.getModeName();

            if (s == null || MathHelper.stringNullOrLengthZero(s))
            {
                s = "Mode " + (id + 1);
            }

            String s1 = mode.getModeDesc();
            if (s1 == null || MathHelper.stringNullOrLengthZero(s1))
            {
            	s1 = "(No description)";
            }
            
            GuiStaffCP.this.fontRendererObj.drawString(s, posX + 2, posY + 1, 16777215);
            GuiStaffCP.this.fontRendererObj.drawSplitString(s1, posX + 2, posY + 12, this.getListWidth(), 8421504);
        }
        
    }    
}