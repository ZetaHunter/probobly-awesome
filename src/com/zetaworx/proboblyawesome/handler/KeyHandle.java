package com.zetaworx.proboblyawesome.handler;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.ClientRegistry;
import net.minecraft.client.settings.KeyBinding;

public class KeyHandle {

    public static KeyBinding ping;
    public static KeyBinding pong;
    public static KeyBinding tscp;


    public static void init() {
    	tscp = new KeyBinding("key.tscp", Keyboard.KEY_GRAVE,		"key.categories.gameplay");
        ping = new KeyBinding("key.ping", Keyboard.KEY_O, 			"key.categories.gameplay");
        pong = new KeyBinding("key.pong", Keyboard.KEY_P,			"key.categories.gameplay");
        ClientRegistry.registerKeyBinding(tscp);
        ClientRegistry.registerKeyBinding(ping);
        ClientRegistry.registerKeyBinding(pong);
    }

}
