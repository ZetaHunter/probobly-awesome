package com.zetaworx.proboblyawesome.handler;

import com.zetaworx.proboblyawesome.ProboblyAwesomeMod;
import com.zetaworx.proboblyawesome.handler.packet.packets.TestPacket;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerEvent;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class EventHandle {

	@SubscribeEvent
	public void PlayerUpdate(PlayerEvent.LivingUpdateEvent evt) {

		try {
			if (evt.entityLiving instanceof EntityPlayer) {
				EntityPlayer p = (EntityPlayer) evt.entityLiving;
				ItemStack weapon = p.getCurrentEquippedItem();
				if (weapon != null)
					/* ==== ARCHMAGE EFFECTS ==== */
					if (weapon.getItem().equals(ProboblyAwesomeMod.theStaff)) {
						if (p.isBurning()) {
							if (p.getHealth() < 8)
								p.setHealth(8);
							p.extinguish();
						}
						p.fallDistance = 0;
						if (p.getAir() < 20)
							p.setAir(20);
						p.curePotionEffects(new ItemStack(Items.milk_bucket, 1));
					}
			}
		} catch (Exception e) {
			FMLLog.warning("Could not execute Magic staff's event Effects! Please report to mod author.");
			FMLLog.warning("Exception: " + e.getMessage());
		}

	}
	
	@SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event) {
    	if(KeyHandle.tscp.isPressed())
    	{
            EntityPlayer pl = Minecraft.getMinecraft().thePlayer;
            pl.openGui(ProboblyAwesomeMod.instance, 0, pl.worldObj, 0, 0, 0);
    	}
        if(KeyHandle.ping.isPressed())
        {
            System.out.println("ping");
            ProboblyAwesomeMod.instance.packets.sendToServer(new TestPacket(10));        
        }
        if(KeyHandle.pong.isPressed())
        {
            System.out.println("pong");
            ProboblyAwesomeMod.instance.packets.sendToServer(new TestPacket(99));
        }
    }
    
}
