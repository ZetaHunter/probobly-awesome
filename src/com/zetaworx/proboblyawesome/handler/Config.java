package com.zetaworx.proboblyawesome.handler;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.zetaworx.proboblyawesome.ProboblyAwesomeMod;
import com.zetaworx.proboblyawesome.modes.Mode;
import com.zetaworx.proboblyawesome.modes.ModeRegistry;

import cpw.mods.fml.common.FMLLog;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

public class Config {
	
	private Configuration config;
	
	static public Config instance;
	
	public Config(File file) {
		this();
		load(file);

	}
	
	public Config() {
		instance = this;
	}
	
	public void load(File file)	{
		config = new Configuration(file);
		config.load();		
	}
	
	public void configMain() {
	}
	
	public void configModes() {
		ModeRegistry modeReg = ProboblyAwesomeMod.instance.modes;
		Iterator<Entry<Integer, Mode>> modes =  modeReg.getEntrySet().iterator();
		while (modes.hasNext()) {
			Entry<Integer, Mode> mode = modes.next();
			Map<String, Property> options = mode.getValue().getOptions();
			Iterator<Entry<String, Property>> optionIterator = options.entrySet().iterator();
			while (optionIterator.hasNext()) {
				Entry<String, Property> option = optionIterator.next();
				switch (option.getValue().getType()) {
					case BOOLEAN:
						option.setValue(config.get("modes", mode.getKey() + ":" + option.getKey() , option.getValue().getBoolean(true)));
						break;
					case DOUBLE:
						option.setValue(config.get("modes", mode.getKey() + ":" + option.getKey() , option.getValue().getDouble(0)));
						break;
					case INTEGER:
						option.setValue(config.get("modes", mode.getKey() + ":" + option.getKey() , option.getValue().getInt(0)));
						break;
					case STRING:
						option.setValue(config.get("modes", mode.getKey() + ":" + option.getKey() , option.getValue().getString()));
						break;
					default:
						FMLLog.warning("Invalid type: %s", option.getValue().getType().toString());
						break;
				}
			}
		}

	}
	
	public void configGeneral() {
		ModeRegistry modeReg = ProboblyAwesomeMod.instance.modes;
		Iterator<Entry<String, Property>> genOpIter = modeReg.getOptions().entrySet().iterator();
		while (genOpIter.hasNext())	{
			Entry<String, Property> option = genOpIter.next();
			switch (option.getValue().getType()) {
				case BOOLEAN:
					option.setValue(config.get("modes_general", option.getKey(), option.getValue().getBoolean(true)));
					break;
				case DOUBLE:
					option.setValue(config.get("modes_general", option.getKey(), option.getValue().getDouble(0)));
					break;
				case INTEGER:
					option.setValue(config.get("modes_general", option.getKey(), option.getValue().getInt(0)));
					break;
				case STRING:
					option.setValue(config.get("modes_general", option.getKey(), option.getValue().getString()));
					break;
				default:
					FMLLog.warning("Invalid type: %s", option.getValue().getType().toString());
					break;
			}
		}
	}
	
	public void save() {
		config.save();
	}


}
