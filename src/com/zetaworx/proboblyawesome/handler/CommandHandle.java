package com.zetaworx.proboblyawesome.handler;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class CommandHandle implements ICommand
{
	private List<String> aliases;
	public CommandHandle()
	{
		this.aliases = new ArrayList<String>();
		this.aliases.add("probobly");
		this.aliases.add("awesome");
		this.aliases.add("proboblyawesome");
	}
	
	@Override
	public String getCommandName()
	{
		return "pa";
	}
	
	@Override
	public String getCommandUsage(ICommandSender icommandsender)
	{
		return "pa <text/help>";
	}
	
	@Override
	public List<String> getCommandAliases()
	{
		return this.aliases;
	}
	
	@Override
	public void processCommand(ICommandSender icommandsender, String[] astring)
	{
		if(astring.length == 0)
		{
			icommandsender.addChatMessage(new ChatComponentText("Invalid Arguments. Usage: " + this.getCommandUsage(icommandsender)));
			return;
		}
		if (astring[0] == "help")
		{
			icommandsender.addChatMessage(new ChatComponentText("Usage: " + this.getCommandUsage(icommandsender)));
			return;
		} else {
		ChatComponentText msg = new ChatComponentText("Output: [");
		for (int i = 0;i < astring.length; ++i)
		{
			msg.appendText(" " + astring[i]);
		}
		msg.appendText(" ]");
		icommandsender.addChatMessage(msg);	
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender icommandsender)
	{
	return true;
	}
	
	@Override
	public List<?> addTabCompletionOptions(ICommandSender icommandsender,
	String[] astring)
	{
	return null;
	}
	
	@Override
	public boolean isUsernameIndex(String[] astring, int i)
	{
	return false;
	}
	
	@Override
	public int compareTo(Object o)
	{
	return 0;
	}
}