package com.zetaworx.proboblyawesome.handler.packet.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

import com.zetaworx.proboblyawesome.ProboblyAwesomeMod;
import com.zetaworx.proboblyawesome.handler.packet.AbstractPacket;

import cpw.mods.fml.common.FMLLog;

public class TestPacket extends AbstractPacket {

	int testData = 0;
	
	public TestPacket(int d) {
		testData = d;
	}
	
	public TestPacket() {
		this(0);
	}

	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
		buffer.writeInt(testData);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
		testData = buffer.readInt();
	}

	@Override
	public void handleClientSide(EntityPlayer player) {
		FMLLog.info("[Client] TestData is: " + testData);
	}

	@Override
	public void handleServerSide(EntityPlayer player) {
		FMLLog.info("[Server] TestData is: " + testData);
		ProboblyAwesomeMod.instance.packets.sendTo(new TestPacket(this.testData), (EntityPlayerMP) player);
	}

}
