package com.zetaworx.proboblyawesome.handler.packet.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;

import com.zetaworx.proboblyawesome.handler.packet.AbstractPacket;

public class ThrowPlayerPacket extends AbstractPacket {

	int playerID;
	double epx, epy, epz;
	
	public ThrowPlayerPacket()
	{
		
	}
	
	public ThrowPlayerPacket(int p, double x, double y, double z)
	{
		playerID = p;
		epx = x;
		epy = y;
		epz = z;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
		buffer.writeInt(playerID);
		buffer.writeDouble(epx);
		buffer.writeDouble(epy);
		buffer.writeDouble(epz);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer) {
		playerID = buffer.readInt();
		epx = buffer.readDouble();
		epy = buffer.readDouble();
		epz = buffer.readDouble();
	}

	@Override
	public void handleClientSide(EntityPlayer player) {
        EntityPlayer entity = (EntityPlayer) player.worldObj.getEntityByID(playerID);

        if (entity != null)
        {
        	double x = 0,y = 0,z = 0;
        	if (entity.moveForward != 0 || entity.moveStrafing != 0)
        	{
        	x = entity.posX - entity.lastTickPosX; x*=1.5;
        	z = entity.posZ - entity.lastTickPosZ; z*=1.5;
        	if (entity.isSprinting())
        		{
        		x *= 2;
        		z *= 2;
        		}
        	}
        	else 
        	{
        		x = 0;
        		z = 0;
        	}
        	y = (entity.onGround?1D:1.5D);
            entity.setVelocity(x, y, z);
        }
	}

	@Override
	public void handleServerSide(EntityPlayer player) {
		//This is client side packet.
	}

}
