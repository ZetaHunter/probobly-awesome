package com.zetaworx.proboblyawesome.handler.proxy;

import net.minecraftforge.client.MinecraftForgeClient;

import com.zetaworx.proboblyawesome.ProboblyAwesomeMod;
import com.zetaworx.proboblyawesome.entity.EntityMagicBlock;
import com.zetaworx.proboblyawesome.handler.KeyHandle;
import com.zetaworx.proboblyawesome.renderer.RendererMagicBlock;
import com.zetaworx.proboblyawesome.renderer.RendererStaff;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLLog;

public class ClientProxy extends CommonProxy {
	@Override
	public void registerRendering() {
		FMLLog.info("Client proxy renderer registrar reporting for duty.");

		RenderingRegistry.registerEntityRenderingHandler(EntityMagicBlock.class, new RendererMagicBlock());
		MinecraftForgeClient.registerItemRenderer(ProboblyAwesomeMod.theStaff, new RendererStaff());
	}
	@Override
	public void registerKeybinding()
	{
		KeyHandle.init();
	}
}
