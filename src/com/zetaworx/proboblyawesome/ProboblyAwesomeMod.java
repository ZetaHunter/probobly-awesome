package com.zetaworx.proboblyawesome;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import com.zetaworx.proboblyawesome.entity.EntityMagicBlock;
import com.zetaworx.proboblyawesome.handler.CommandHandle;
import com.zetaworx.proboblyawesome.handler.Config;
import com.zetaworx.proboblyawesome.handler.EventHandle;
import com.zetaworx.proboblyawesome.handler.gui.GuiHandler;
import com.zetaworx.proboblyawesome.handler.packet.PacketPipeline;
import com.zetaworx.proboblyawesome.handler.packet.packets.TestPacket;
import com.zetaworx.proboblyawesome.handler.packet.packets.ThrowPlayerPacket;
import com.zetaworx.proboblyawesome.handler.proxy.CommonProxy;
import com.zetaworx.proboblyawesome.item.ItemStaff;
import com.zetaworx.proboblyawesome.modes.Mode;
import com.zetaworx.proboblyawesome.modes.ModeRegistry;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = ProboblyAwesomeMod.MODID, version = ProboblyAwesomeMod.VERSION)
public class ProboblyAwesomeMod
{
    public static final String MODID = "ProboblyAwesome";
    public static final String VERSION = "0.0.1";
    public static final String RESPREFIX = "zetaworx:";
    
    public ModeRegistry modes;
    public static Item theStaff;
    private static int _lastUniqueID;
    @SidedProxy(serverSide = "com.zetaworx.proboblyawesome.handler.proxy.CommonProxy", clientSide = "com.zetaworx.proboblyawesome.handler.proxy.ClientProxy")
	public static CommonProxy proxy;
    public PacketPipeline packets;
    public Config config;
    //@Instance darn crashes.
    public static ProboblyAwesomeMod instance;
            
	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		instance = this;
    	modes = new ModeRegistry();
    	Mode.registerModes(modes);
    	config = new Config(e.getSuggestedConfigurationFile());
    	config.configMain();
    	config.configModes();
    	config.configGeneral();
    	config.save();
		theStaff = new ItemStaff(500);
		theStaff.setMaxStackSize(1);
		theStaff.setMaxDamage(300);
		theStaff.setUnlocalizedName("theStaff");
		theStaff.setCreativeTab(CreativeTabs.tabCombat);
		GameRegistry.registerItem(theStaff,theStaff.getUnlocalizedName(), MODID);
    	packets = new PacketPipeline("ZHPA");
    	packets.initialise();
    	packets.registerPacket(TestPacket.class);
    	packets.registerPacket(ThrowPlayerPacket.class);
	}
	
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	GameRegistry.addShapelessRecipe(new ItemStack(theStaff), Items.stick, Items.stick);
		EntityRegistry.registerModEntity(EntityMagicBlock.class, "Elemental", this.getUniqueID(), this, 250, 1, true);
    	proxy.registerRendering();
    	proxy.registerKeybinding();
		FMLCommonHandler.instance().bus().register(new EventHandle());
    	MinecraftForge.EVENT_BUS.register(new EventHandle());

    	NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	packets.postInitialise();
    }
    
    public int getUniqueID() {
    return _lastUniqueID++;
    }
    
    @EventHandler
    public void serverLoad(FMLServerStartingEvent event)
    {
    	event.registerServerCommand(new CommandHandle());
    }
    
}
